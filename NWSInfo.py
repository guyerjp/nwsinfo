#!/usr/bin/python3

# This library is used to interface with the US National Weather Service API.
# Created by Jason Guyer in 2023
# Example usage
#
# import get_weather
# new_day = get_weather.build_link('33.4435','-86.9944')
# print(new_day.get_current_conditions())
# print(new_day.get_forecast())
# print(f'City: {new_day.city}')

import json, requests

class build_link:

	def __init__(self, longitude, latitude):
		self.longitude = longitude
		self.latitude = latitude
		self.forecast_link = None
		self.hourly_forecast_link = None
		self.current_conditions_link = None
		self.city = None
		self.state = None
		self.degree_symbol = u"\u00b0"
		self.build_links()


	def build_links(self):
		first_request_string = f'https://api.weather.gov/points/{self.longitude},{self.latitude}'
		first_request_response = requests.get(first_request_string)
		first_request_json = json.loads(first_request_response.text)
		self.forecast_link = first_request_json['properties']['forecast']
		self.hourly_forecast_link = first_request_json['properties']['forecastHourly']
		self.city = first_request_json['properties']['relativeLocation']['properties']['city']
		self.state = first_request_json['properties']['relativeLocation']['properties']['state']
		zone_url = first_request_json['properties']['forecastZone']
		zone_split = zone_url.split("/")[-1]
		self.current_conditions_url = f'https://api.weather.gov/zones/forecast/{zone_split}/observations'


	def get_forecast_link(self):
		return self.forecast_link


	def get_hourly_forecast_link(self):
		return self.hourly_forecast_link


	def get_current_conditions_link(self):
		return self.current_conditions_url


	def get_city(self):
		return self.city


	def get_state(self):
		return self.state


	def set_coordinates(self, latitude, longitude):
		self.latitude = latitude
		self.longitude = longitude
		self.build_links()


	def get_forecast(self):
		forecast = []
		try:
			forecast_request = requests.get(self.forecast_link)
		except requests.exceptions.Timeout:
			print('Error, Timeout: The connection has timed out')
		except requests.exceptions.TooManyRedirects:
			print('Error, Too Many Redirects: Try a different URL')
		except requests.exceptions.ConnectionError:
			print('Error, Connection Error: There was an error connecting to the remote host')
		except request.exceptions.HTTPError:
			print('Error, HTTP Error: An HTTP error occurred')
		except request.exceptions.RequestException as e:
			raise SystemExit(e)
		if forecast_request:
			forecast_request_json = json.loads(forecast_request.text)
			daily = forecast_request_json['properties']['periods']
			# print('\n')
			for item in daily:
				daily_conditions = {}
				name = item['name']
				temperature = item['temperature']
				temp_unit = item['temperatureUnit']
				temp_trend = item['temperatureTrend']
				humidity_nopercent = item['relativeHumidity']['value']
				percipitation_probability_nopercent = item['probabilityOfPrecipitation']['value']
				wind_speed = item['windSpeed']
				wind_direction = item['windDirection']
				short_forecast = item['shortForecast']
				detailed_forecast = item['detailedForecast']
				daily_conditions.update({'name':name, 'temperature':temperature, 'temperatureUnit': temp_unit, 'temperatureTrend': temp_trend, 'humidity':humidity_nopercent, 'percipitation_probability':percipitation_probability_nopercent, 'windspeed':wind_speed, 'winddirection':wind_direction, 'shortforecast':short_forecast, 'detailedforecast':detailed_forecast})
				forecast.append(daily_conditions)
			return forecast
		else:
			None


	def get_current_conditions(self):
		conditions = {}
		try:
			conditions_request = requests.get(self.current_conditions_url)
		except requests.exceptions.Timeout:
			print('Error, Timeout: The connection has timed out')
		except requests.exceptions.TooManyRedirects:
			print('Error, Too Many Redirects: Try a different URL')
		except requests.exceptions.ConnectionError:
			print('Error, Connection Error: There was an error connecting to the remote host')
		except request.exceptions.HTTPError:
			print('Error, HTTP Error: An HTTP error occurred')
		except request.exceptions.RequestException as e:
			raise SystemExit(e)
		if conditions_request:
			conditions_request_json = json.loads(conditions_request.text)
			current = conditions_request_json['features']
			current_time = current[0].get("properties")
			short_description = current_time['textDescription']
			temperature = self.c_to_f(current_time['temperature']['value'])
			dewpoint = self.c_to_f(current_time['dewpoint']['value'])
			wind_speed = current_time['windSpeed']['value']
			wind_direction = current_time['windDirection']['value']
			pressure = current_time['barometricPressure']['value']
			visibility = self.kilometers_to_miles(current_time['visibility']['value'])
			humidity = round(current_time['relativeHumidity']['value'], 2)
			wind_chill = self.c_to_f(current_time['windChill']['value'])
			heat_index = round(self.c_to_f(current_time['heatIndex']['value']), 2)
			conditions.update({'shortdescription':short_description, 'temperature':temperature, 'dewpoint':dewpoint, 'windspeed':wind_speed, 'winddirection':wind_direction, 'pressure':pressure, 'visibility':visibility, 'humidity':humidity, 'windchill':wind_chill, 'heatindex':heat_index})
			return conditions
		else:
			None


	def c_to_f(self, celsius):
		if celsius == None:
			return 'N/A'
		else:
			fahrenheit = (celsius * 9/5) + 32
			return fahrenheit


	def kilometers_to_miles(self, kilometers):
		if kilometers == None:
			return 'N/A'
		else:
			miles = (kilometers/1000) * 0.62137119
			return round(miles, 2)
